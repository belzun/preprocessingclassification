#busqueda en grill de los parámetros optimos para
# el ruido

rm(list = ls())
source("main/load.R")

trainPath = "particionesTrain/part-1-tra.csv"
testPath  = "particionesTrain/part-1-tst.csv"

variables = expand.grid(s = 2:4, p = c(0.001, 0.01, 0.05, 0.1), y=c(0.1,0.2,0.5,0.6))

resultadosTotales_1nn = data.frame(colnames(c("percent","kappa","s","p","y")))
resultadosTotales_ripper = data.frame(colnames(c("percent","kappa","s","p","y")))

for (i in 1:nrow(variables)){
  
  r = preprocesamientoInicial(trainPath, testPath)
  train = r$train
  test = r$test
  
  
  #Imputacion
  train = imputacion_knni(train,3)
  test = imputacion_knni(test,3)
  
  train = imputacion_moda(train)
  test = imputacion_moda(test)
  
  
  
  # Ruido
  s=variables[i,"s"]
  p=variables[i,"p"]
  y=variables[i,"y"]
  train = deleteNoise(train, s=s, p=p, y=y)
  
  
  
  #Normalizar
  train[-getColumnasFactor(train)] = 
    scale(train[-getColumnasFactor(train)],
          center = TRUE, scale = TRUE)
  test[-getColumnasFactor(test)] =
    scale(test[-getColumnasFactor(test)],
          center = TRUE, scale = TRUE)
  
  
  result_1nn = run_1nn(train, test)
  result_ripper = run_ripper(train, test)
  
  resultadosTotales_1nn = rbind(resultadosTotales_1nn, 
                                data.frame( percent = result_1nn$percent,
                                            kappa = result_1nn$kappa,
                                            s=variables[i,"s"],
                                            p=variables[i,"p"],
                                            y=variables[i,"y"]))
  
  resultadosTotales_ripper = rbind(resultadosTotales_ripper, 
                                   data.frame( percent = result_ripper$percent,
                                               kappa = result_ripper$kappa,
                                               s=variables[i,"s"],
                                               p=variables[i,"p"],
                                               y=variables[i,"y"]))
  
  write(as.character(i),file="avance.txt",append=TRUE)
}

beep()

