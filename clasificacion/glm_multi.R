#' Devuelve el resultado de la clasificacion con GLM
#' 
#' @param train: conjunto de entrenamiento
#' @param test: conjunto de prueba
#' @param modelo: modelo en string
#' @param clase: nombre de la variable que contiene la clase en train
#' 
#' @return Clasificacion del conjunto test
glm_multi4<-function(train,test,modelo="y~.",clase="y"){
  columna<-which(names(train)==clase)
  data0<-train
  data0[,columna]<-ifelse(train[,columna]==0,1,0)
  data1<-train
  data1[,columna]<-ifelse(train[,columna]==1,1,0)
  data2<-train
  data2[,columna]<-ifelse(train[,columna]==2,1,0)
  
  model0 <-glm(as.formula(modelo), family="binomial", data= data0)
  model1 <-glm(as.formula(modelo), family="binomial", data= data1)
  model2 <-glm(as.formula(modelo), family="binomial", data= data2)
  
  a0 <- round(predict(model0, newdata = test, type="response"))
  a1 <- round(predict(model1, newdata = test, type="response"))
  a2 <- round(predict(model2, newdata = test, type="response"))
  
  dat<-rep(-1,nrow(test))
  dat<-ifelse(a0==1,0,dat)
  dat<-ifelse(a1==1,1,dat)
  dat<-ifelse(a2==1,2,dat)
  dat<-ifelse(dat==-1,3,dat)
  
  dat
}
