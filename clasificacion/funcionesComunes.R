#' Guarda la predicción realizada en un archivo cvs para subirlo fácilmente a Kaggle
#' 
#' @param Prediction: resultado obtenido como vector o lista
#' 
#' @param fileTitle: archivo en el que se guarda el resultado (output.cvs por defecto)
outputToKaggle <- function(Prediction, fileTitle = "output.cvs"){
  Id <- 1:length(Prediction)
  outputDataset <- data.frame(Id, Prediction)
  write.csv(outputDataset,
            file = fileTitle,
            row.names = FALSE,
            quote = FALSE)
}