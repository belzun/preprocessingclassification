#'Ejecuta un modelo de forma concurrente
#'
#'@param funcionModelo: funcion del modelo a ejecutar,
#' tiene que tener 2 parametros, el path del train y el path del test
#'
#'@param nParticiones: indica el número de carticiones con el que se va a hacer
#'cross validation
#'
#'@param seed: semilla
#'
#'@param recrearParticiones: si siempre usas el mismo numero de particiones con
#' la misma semilla y no quieres que pierda tiempo en recrearlas ponerlo en FALSE
#'
#'@return: los resultados en una matriz, imprime la media de % de acierto y kappa 
#'
cv = function(funcionModelo, nParticiones = 5, seed = 0, recrearParticiones = TRUE){
  
  if(recrearParticiones){
    particiones(nParticiones = nParticiones, seed = seed)
  }
    
  trainFiles = sapply(1:nParticiones, function(i) paste("particionesTrain/part-",i,"-tra.csv",sep=""))
  testFiles = sapply(1:nParticiones, function(i) paste("particionesTrain/part-",i,"-tst.csv",sep=""))

  
  #TODO: Hacer esto concurrente  
  resultados = sapply(1:nParticiones,
                     function(i) funcionModelo(trainFiles[i], testFiles[i]) )
  
  print(paste("media percent",mean(as.numeric(resultados[1,]))))
  print(paste("media kappa",mean(as.numeric(resultados[2,]))))
  
  resultados
}