#' Ejecuta el algoritmo rpart
#' No discretiza nada
#' 
#' @param train: datos de entrenamiento
#' 
#' @param test: datos de test, la ultima columna que debe ser la etiqueta
#'
#' @param formula: formula que indica como debe ser la clasificacion, opcional
#'
#' @param showResult: variable opcional por si no quieres que muestre los
#' resultados
#' 
#' @return lista con los parametros:
#'  percent: porcentaje de acierto
#'  kappa: kappa conseguida
#'  table: tabla de confusion
#'  
#' @example result = run_rpart(train, test)
#'  
run_rpart = function(train, test, formula = y~., showResult=TRUE){
  
  require(irr)
  require(rpart)
  
  fit <- rpart(formula, train)
  predict_test = predict(fit, test, type="class")
 
  
   
  # Calcular y mostrar resultados
  result = list()
  tagIndex = dim(train)[2]
  clave.test = test[,tagIndex]
  
  result$percent = sum(predict_test == clave.test) / length(predict_test)
  result$kappa = kappa2(data.frame(predict_test, clave.test))$value
  result$table = table(predict_test, clave.test)
  
  printResults_rpart(result)
  
  result
}


printResults_rpart = function(result){
  print("Acierto:")
  print(result$percent)
  print("Kappa:")
  print(result$kappa)
  print("Tabla de confusion:")
  print(result$table)
}


#' Entrena el modelo rpart con los datos train,
#' predice los datos test y lo guarda en el lugar indicado
#'
#' @param train: Datos de entrenamiento
#' @param test: Datos a predecir
#' @param formula: formula que indica como debe ser la clasificacion, opcional
#' @param fileTitle: lugar donde se guardan los datos en formato
#' necesario para kaggle
#' 
run_rpart_kaggle = function(train, test, formula = y~. , fileTitle = "output.cvs"){
  
  require(RWeka)
  
  require(rpart)
  
  fit <- rpart(formula, train)
  predict_test = predict(fit, test, type="class")
  
  outputToKaggle(predict_test, fileTitle)
}
