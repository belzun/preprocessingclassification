#Script análisis exploratorio Nuño
#Lectura de los datos
train <- read.csv(file="my_dataset_train.csv", header=TRUE, sep=",")
test <- read.csv(file="my_dataset_test.csv", header=TRUE, sep=",")

trainCor = train[-c(1,15,18,52,62,64)]

corData = cor(dataCor, method = 'pearson', use = 'pairwise.complete.obs')[dim(dataCor)[2],-dim(dataCor)[2]]
top10CorrData = order(abs(corData), decreasing = TRUE)[1:10]
corData[top10CorrData]

any(apply(train, 2, is.na))

#Vamos a realizar una análisis de las variables dicretas
table(train$x0)
table(train$x14)
table(train$x17)
table(train$x51)
table(train$x61)
table(train$x63)


train = na.omit(train)
filas.factor = c(1,15,18,52,62,64,76)

train[-filas.factor] =
  scale(train[-filas.factor], center = TRUE, scale = TRUE)



