#SE DEBE EJECUTAR IMPUTACION.R PREVIAMENTE

datosImputados2
datosImpCont = datosImputados2[-c(1,15,18,52,62,64,76)]

# se analizan los datos en busca de anomalias. El grafico
# resultante muestra en rojo los datos considerados considerados
# como anomalos
booleanRes = c()
valorIter = dim(datosImpCont)[2]/10
for (i in c(1:valorIter)){
  primero <- ((i-1)*10)+1
  segundo <- i*10
  resultados <- mvoutlier::uni.plot(datosImpCont[primero:segundo])
  if(i==1) booleanRes<-resultados$outliers
  else booleanRes <- resultados$outliers|booleanRes
}

#Nos quedamos con las instancias sin outliers.
datosFinales <- datosImpCont[!booleanRes, ]

indices_quitados <- which(booleanRes==TRUE)
factoresdf <- datosImputados2[c(1,15,18,52,62,64,76)]
factoresdf <- factoresdf[-indices_quitados,]
datosFinales <- cbind(datosFinales, factoresdf)
