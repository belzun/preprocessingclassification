#Se va a tratar el preprocesamiento y sus diferentes métodos.

train = read.csv("my_dataset_train.csv", na.strings=c ( "." , "NA" , "" , "?" ) )
test = read.csv("my_dataset_test.csv", na.strings=c ( "." , "NA" , "" , "?" ) )

res <- apply(train, 1, function(x) sum(is.na(x))) / ncol(train) * 100

#Mal almacena aquellas variables con un porcentaje mayor a 1%
#El dataset tiene entre un 1 y un 2% de valores perdidos
#por lo que no se ve necesario limpiar ninguna fila.
mal <- (res > 1)
table(mal)


require(VIM)
#Se genera el grafico de distribucion de datos perdidos. Solo
#Se consideran las variables con datos perdidos
ValoresPerdidos <- VIM::aggr(train, col=c('blue','red'), numbers=TRUE, 
                  sortVars=TRUE, labels=names(data), cex.axis=.5, 
                  gap=1, ylab=c("Grafico de datos perdidos","Patron"))



#IMPUTACION DE DATOS MEDIANTE MICE
install.packages("mice")
library(mice)
library(lattice)

completos <- mice::ncc(train)
incompletos <- mice::nic(train)
cat("Datos completos: ",completos, " e incompletos: ",incompletos,"\n")

imputados <- mice(train, m=5, meth="pmm")
datosImputados <- mice::complete(imputados)

completos <- mice::ncc(datosImputados)
incompletos <- mice::nic(datosImputados)
cat("Datos completos: ",completos, " e incompletos: ",incompletos,"\n")

imputados <- mice(datosImputados, m=5, meth="pmm")
datosImputados2 <- mice::complete(imputados)

completos <- mice::ncc(datosImputados2)
incompletos <- mice::nic(datosImputados2)
cat("Datos completos: ",completos, " e incompletos: ",incompletos,"\n")

mean(data$x9, na.rm=TRUE)


#IMPUTACION DE DATOS  ROB COMPOSITIONS
#Dado que Rob Compositions solo sirve para variables continuas vamos a eliminar las variables discretas en un nuevo dataset.
require(robCompositions)

trainCor = train[-c(1,15,18,52,62,64)]
completos2 <- mice::ncc(trainCor)
incompletos2 <- mice::nic(trainCor)
cat("Datos completos sin variables discretas: ",completos2, " e incompletos: ",incompletos2,"\n")

completos <- mice::ncc(train)
incompletos <- mice::nic(train)
cat("Datos completos: ",completos, " e incompletos: ",incompletos,"\n")
incompletos-incompletos2
#Hay 24 valores perdidos en las variables discretas.

#Ejecución de robCompositions
imputadosRC <- robCompositions::impKNNa(trainCor, k=1)
datosImputadosRob = imputadosRC$xImp

# Error in quantile.default(d, k/length(d)) : 
#   missing values and NaN's not allowed if 'na.rm' is FALSE
# In addition: Warning message:
# In cenLR(rbind(x[mi, m1, drop = FALSE], x[i, m1])) : NaNs produced



#OUTLIERS

#Trabajamos con datosImputados2, una vez ya imputados los valores.

datosImputados2
datosImpCont = datosImputados2[-c(1,15,18,52,62,64,76)]

# se analizan los datos en busca de anomalias. El grafico
# resultante muestra en rojo los datos considerados considerados
# como anomalos
booleanRes = c()
valorIter = dim(datosImpCont)[2]/10
for (i in c(1:valorIter)){
  primero <- ((i-1)*10)+1
  segundo <- i*10
  resultados <- mvoutlier::uni.plot(datosImpCont[primero:segundo])
  if(i==1) booleanRes<-resultados$outliers
  else booleanRes <- resultados$outliers|booleanRes
}

#Nos quedamos con las instancias sin outliers.
datosFinales <- datosImpCont[!booleanRes, ]
