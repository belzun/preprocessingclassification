#IMPUTACION DE DATOS MEDIANTE MICE
library(mice)
library(lattice)

train = read.csv("my_dataset_train.csv", na.strings=c ( "." , "NA" , "" , "?" ) )
test = read.csv("my_dataset_test.csv", na.strings=c ( "." , "NA" , "" , "?" ) )

completos <- mice::ncc(train)
incompletos <- mice::nic(train)
cat("Datos completos: ",completos, " e incompletos: ",incompletos,"\n")

imputados <- mice(train, m=5, meth="pmm")
datosImputados <- mice::complete(imputados)

completos <- mice::ncc(datosImputados)
incompletos <- mice::nic(datosImputados)
cat("Datos completos: ",completos, " e incompletos: ",incompletos,"\n")

imputados <- mice(datosImputados, m=5, meth="pmm")
datosImputados2 <- mice::complete(imputados)

completos <- mice::ncc(datosImputados2)
incompletos <- mice::nic(datosImputados2)
cat("Datos completos: ",completos, " e incompletos: ",incompletos,"\n")

#VARIABLE FINAL DE SALIDA TRAIN:
datosImputados2

#AHORA VAMOS A TRATAR EL DATASET TEST
completos <- mice::ncc(test)
incompletos <- mice::nic(test)
cat("Datos completos: ",completos, " e incompletos: ",incompletos,"\n")

imputadosTest <- mice(test, m=5, meth="pmm")
datosImputadosTest <- mice::complete(imputadosTest)

imputadosTest2 <- mice(datosImputadosTest, m=5, meth="pmm")
datosImputadosTest2 <- mice::complete(imputadosTest2)


completos <- mice::ncc(datosImputadosTest2)
incompletos <- mice::nic(datosImputadosTest2)
cat("Datos completos: ",completos, " e incompletos: ",incompletos,"\n")
