#pruebas para normalizacion

source("preprocesamiento/Normalizacion.R")


trainn<-read.csv("my_dataset_train.csv", na.strings=c ( "." , "NA" , "" , "?" ) )

datt<-na.omit(trainn)
datt<-datt[1:100,1:5]

min(datt) #-19.05
max(datt) # 19.01

minmax(datt,0,1)

ddd<-matrix(1:21,ncol=3)
minmaxTodo(ddd,0,1)

minmaxTodo(datt,0,1)
minmaxTodo(datt,0,1,c(2))
datt[c(1,2)]


zeroMeanTodo(datt)
zeroMeanTodo(datt,c(2))


escalaDecimalTodo(datt)

datt<-as.data.frame(datt)
names(datt)[!sapply(datt, class)=="factor"]
escalaDecimalTodo(datt)
escalaDecimalTodo(datt,c(2,3,4,5))

escalaDecimal(datt[c(3)])
escalaDecimal(datt[c(2,3)])
