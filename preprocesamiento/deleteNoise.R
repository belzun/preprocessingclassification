#'Devuelve los datos sin ruido
#'La variable de salida tiene que ser factor
#'
#'@param data: dataset
#'@param s: Criterio de parada del algoritmo
#'@param seed: Semilla
#'
deleteNoise = function(data, s = 3, p=0.1, y = 0.2, seed = 1){
  
  library(NoiseFiltersR)
  
  set.seed(seed)
  
  out = IPF(y~., data=data, s=s, p=p, y=y)
  
  # Devolver los datos sin el ruido
  data[setdiff(1:nrow(data),out$remIdx),]
  
  
}