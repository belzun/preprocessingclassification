#' Función que realiza una transformación de los datos y la normalización.
#' 
#' @param datos: Conjunto de datos que vamos a transformar
#' 
#' @return Datos transformados
transformacion = function(datos){
  require(mice)
  require(caret)
  datosContinuos = datos[-columnas.factor]
  valoresPreprocesados <- caret::preProcess(datosContinuos,method=c("center","scale"))
  valoresTransformados <- predict(valoresPreprocesados,datosContinuos)
  datosFinales <- cbind(valoresTransformados,datos[columnas.factor])

return(datosFinales)
}

