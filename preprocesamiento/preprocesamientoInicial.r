
preprocesamientoInicial = function(trainPath, testPath){

  # Leer datos 
  train = read.csv(trainPath, na.strings=c ( "." , "NA" , "" , "?" ) )
  test = read.csv(testPath, na.strings=c ( "." , "NA" , "" , "?" ) )
  
  #Ponemos la salida "y" como factor
  train[["y"]] = factor(train[["y"]])
  #Si existe la columna y en el test se pasa a factor
  if("y" %in% colnames(test) ) {
    test [["y"]] = factor(test [["y"]])
  }
  
  
  #Las variables x41 y x48 estan correlacionada a 1, quitamos una
  train = train[,-49]
  test = test[,-49]
  
  retorno = list()
  retorno$train = train
  retorno$test = test
  
  retorno
}


#Funciones

# Indice de las columnas que son factores
getColumnasFactor =function(data){
  as.integer(which(sapply(data, is.factor)))
}


#Todos los indices que son factores sin contar el tag
getColumnasFactorSinTag = function(data){
  columnas.factor = getColumnasFactor(data)
  indexY = getIndiceColumnaY(data)
  columnas.factor[!(columnas.factor %in% indexY)]
}


#Indice de la columna de salida
getIndiceColumnaY = function(data, labelTag = "y"){
  which(names(data) == labelTag)
}

