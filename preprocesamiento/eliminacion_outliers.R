#' Función que elimina los outliers en nuestro dataset.
#' 
#' @param datos: Conjunto de datos del que vamos a eliminar outliers
#' 
#' @return Conjunto de datos sin outliers
eliminacion_outliers = function(datos){
  
  columnas.factor = getColumnasFactor(datos)
  datosImpCont = datos[-getColumnasFactorSinTag(datos)]
  
  booleanRes = c()
  valorIter = dim(datosImpCont)[2]/10
  for (i in c(1:valorIter)){
    primero <- ((i-1)*10)+1
    segundo <- i*10
    resultados <- mvoutlier::uni.plot(datosImpCont[primero:segundo])
    if(i==1) booleanRes<-resultados$outliers
    else booleanRes <- resultados$outliers|booleanRes
  }
  
  #Nos quedamos con las instancias sin outliers.
  datosFinales <- datosImpCont[!booleanRes,]
  
  indices_quitados <- which(booleanRes==TRUE)
  factoresdf <- datos[columnas.factor]
  factoresdf <- factoresdf[-indices_quitados,]
  datosFinales <- cbind(datosFinales, factoresdf)
  
  #La columna "y" queda repetida
  datosFinales <- datosFinales[, !duplicated(colnames(datosFinales))]
  
  return(datosFinales)
}


#' Elimina las filas que contienen outliers y NA
#' 
#' @param data: conjunto de datos con el que trabajar
#' 
#' @param tipoOutlier: tipos de outlier que se quieren eliminar (por defecto normales, con una distancia de 1.5 IQR. Si se se?ala cualquier otra cosa se entiende que son extremos con una distancia de 3 IQR).
#' 
#' @return data: conjunto de datos con las filas que contienen outliers o NA eliminadas
deleteOutliersIQR <- function(data, tipoOutlier = "normal"){
  if (tipoOutlier == "normal"){
    tipoOutlier <- 1.5
  }else{
    tipoOutlier <- 3
  }
  for (i in 1:ncol(data)){
    if (class(data[,i]) != "factor"){
      primer.cuartil <- quantile(data[,i], 0.25, na.rm = TRUE)
      tercer.cuartil <- quantile(data[,i], 0.75, na.rm = TRUE)
      iqr <- IQR(data[,i], na.rm = TRUE)
      extremo.superior <- tercer.cuartil + (tipoOutlier * iqr)
      extremo.inferior <- primer.cuartil - (tipoOutlier * iqr)
      vector.extremo <- ifelse(data[,i] > extremo.superior | data[,i] < extremo.inferior, TRUE, FALSE)
      data <- data[!vector.extremo,]
    }
  }
  return(data)
}


#' Convierte los outliers de cada columna en NA
#' 
#' @param data: conjunto de datos con el que trabajar
#' 
#' @param tipoOutlier: tipos de outlier que se quieren eliminar (por defecto normales, con una distancia de 1.5 IQR. Si se se?ala cualquier otra cosa se entiende que son extremos con una distancia de 3 IQR).
#' 
#' @return data: conjunto de datos con los outliers como NA
outliersToNA <- function(data, tipoOutlier = "normal"){
  if (tipoOutlier == "normal"){
    tipoOutlier <- 1.5
  }else{
    tipoOutlier <- 3
  }
  for (i in 1:ncol(data)){
    if (class(data[,i]) != "factor"){
      primer.cuartil <- quantile(data[,i], 0.25, na.rm = TRUE)
      tercer.cuartil <- quantile(data[,i], 0.75, na.rm = TRUE)
      iqr <- IQR(data[,i], na.rm = TRUE)
      extremo.superior <- tercer.cuartil + (tipoOutlier * iqr)
      extremo.inferior <- primer.cuartil - (tipoOutlier * iqr)
      vector.extremo <- ifelse(data[,i] > extremo.superior | data[,i] < extremo.inferior, TRUE, FALSE)
      data[,i][which(vector.extremo)] <- NA
    }
  }
  return(data)
}